//
//  PLTConfig.m
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "PLTConfig.h"

@implementation PLTConfig

#pragma mark - URLs configuration

+ (NSString *)baseURL {
#if defined(CONFIG_QA)
    return @"QA_URL";
    
#elif defined(CONFIG_STAGE)
    return @"STAGE_URL";
    
#elif defined(CONFIG_PROD)
    return @"PROD_URL";
#endif
}

#pragma mark - Google Analytics

+ (NSString *)googleAnalyticsTrackingID {
#if defined(CONFIG_PROD)
    return @"PROD_ID";
#else
    return @"DEV_ID";
#endif
}

@end
