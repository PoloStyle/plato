//
//  PLTConfig.h
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PLTConfig : NSObject

#pragma mark - URLs configuration

+ (NSString *)baseURL;

#pragma mark - Google Analytics

+ (NSString *)googleAnalyticsTrackingID;

@end
