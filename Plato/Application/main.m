//
//  main.m
//  Plato
//
//  Created by Marc Janga on 09/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PLTAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PLTAppDelegate class]));
    }
}
