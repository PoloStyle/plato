//
//  PLTLocationManager.m
//  B2CBaseComponents
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2014 Polonius. All rights reserved.
//

#import "PLTLocationManager.h"

@implementation PLTLocationManager

#pragma mark - Init -

- (instancetype) init
{
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return self;
}

+ (PLTLocationManager *)sharedManager
{
    static PLTLocationManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Location Services Status -

+ (BOOL) locationServicesEnabled {
    return [CLLocationManager locationServicesEnabled];
}

+ (CLAuthorizationStatus) authorizationStatus {
    return [CLLocationManager authorizationStatus];
}

#pragma mark - Location Manager Delegate -

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([self.delegate respondsToSelector:@selector(locationManagerDidChangeAuthorizationStatus:)]) {
        [self.delegate locationManagerDidChangeAuthorizationStatus:status];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    if(!self.location || newLocation.coordinate.latitude != self.location.coordinate.latitude ||
       newLocation.coordinate.longitude != self.location.coordinate.longitude){
        if ([self.delegate respondsToSelector:@selector(locationManagerDidUpdateLocation:)]) {
            [self.delegate locationManagerDidUpdateLocation:locations.lastObject];
        }
        
        [self setLocation:locations.lastObject];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"Update location failed with error: %@", error);
}

#pragma mark - Location Manager Access -

- (void) startUpdatingLocation {
    [self.locationManager startUpdatingLocation];
}

- (void) stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
}

@end
