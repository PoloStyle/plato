//
//  PLTLocationManager.h
//  B2CBaseComponents
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2014 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define PLTLOCATIONMANAGER PLTLocationManager.sharedManager

@protocol PLTLocationManagerDelegate

@optional

- (void)locationManagerDidUpdateLocation:(CLLocation *)location;
- (void)locationManagerDidChangeAuthorizationStatus:(CLAuthorizationStatus)status;

@end

@interface PLTLocationManager : NSObject <CLLocationManagerDelegate>

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) CLLocationManager *locationManager;

+ (PLTLocationManager *)sharedManager;
+ (BOOL) locationServicesEnabled;
+ (CLAuthorizationStatus) authorizationStatus;

- (void) startUpdatingLocation;
- (void) stopUpdatingLocation;

@end
