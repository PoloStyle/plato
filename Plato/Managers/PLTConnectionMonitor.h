//
//  PLTConnectionMonitor.h
//  ShellB2C
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Reachability/Reachability.h>

typedef void (^PLTConnectionMonitorBlock)(void);

@interface PLTConnectionMonitor : NSObject

@property (nonatomic, readonly) BOOL isOnline;

+ (instancetype)sharedMonitor;

- (NetworkStatus)currentNetworkStatus;

- (void)removeObserver:(id)observer;
- (void)addObserver:(id)observer withReachableBlock:(PLTConnectionMonitorBlock)reachableBlock
                                   unreachableBlock:(PLTConnectionMonitorBlock)unreachableBlock;

/* USAGE
 
 *** Add self as observer for connection updates ***
 
 [[PLTConnectionMonitor sharedMonitor] addObserver: self withReachableBlock:^(void) {
 
    // Handle internet connection available
 
 } unreachableBlock:^(void) {
 
     // Handle internet connection not available
 }];
 
 *** Remove self as observer in dealloc ***
 
 - (void) dealloc {
    [[PLTConnectionMonitor sharedMonitor] removeObserver:self];
 }
 
 */

@end
