//
//  PLTConnectionMonitor.m
//  ShellB2C
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "PLTConnectionMonitor.h"

static NSString *const kKeyDictId           = @"dictidentifier";
static NSString *const kKeyReachableBlock   = @"reachable_block";
static NSString *const kKeyUnreachableBlock = @"unreachable_block";

@interface PLTConnectionMonitor()

@property (nonatomic, strong) Reachability   *reachability;
@property (nonatomic, strong) NSMutableArray *observers;

@end

@implementation PLTConnectionMonitor

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.observers = [NSMutableArray new];
        self.reachability = [Reachability reachabilityForInternetConnection];
        [self startMonitoring];
    }
    return self;
}

#pragma mark - Singleton

+ (instancetype)sharedMonitor
{
    static PLTConnectionMonitor *sharedMonitor = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        sharedMonitor = [self new];
    });
    
    return sharedMonitor;
}

#pragma mark - Methods (Public)

- (void)removeObserver:(id)observer {
    NSValue *observerKey = [NSValue valueWithNonretainedObject:observer];
    for (NSDictionary *dict in self.observers) {
        if ([dict[kKeyDictId] isEqualToValue:observerKey]) {
            [self.observers removeObject:dict];
            break;
        }
    }
}

- (void)addObserver:(id)observer withReachableBlock:(PLTConnectionMonitorBlock)reachableBlock
                                   unreachableBlock:(PLTConnectionMonitorBlock)unreachableBlock {
    
    NSMutableDictionary *observerDict = [NSMutableDictionary new];
    if (reachableBlock) {
        [observerDict setObject:[reachableBlock copy]   forKey:kKeyReachableBlock];
    }
    
    if (unreachableBlock) {
        [observerDict setObject:[unreachableBlock copy] forKey:kKeyUnreachableBlock];
    }
    
    NSValue *observerKey = [NSValue valueWithNonretainedObject:observer];
    [observerDict setObject:observerKey forKey:kKeyDictId];
    
    [self.observers addObject:observerDict];
}

- (NetworkStatus)currentNetworkStatus {
    return [self.reachability currentReachabilityStatus];
}

#pragma mark - Custom accessors

- (BOOL)isOnline {
    return _reachability.isReachable;
}

#pragma mark - Methods (Private)

- (void)startMonitoring {
    __weak PLTConnectionMonitor *weakSelf = self;
    
    // Rechable code block
    _reachability.reachableBlock = ^(Reachability *reach)
    {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            for (NSDictionary* observerDict in weakSelf.observers) {
                PLTConnectionMonitorBlock reachableBlock = [observerDict objectForKey:kKeyReachableBlock];
                if (reachableBlock) {
                    reachableBlock();
                }
            }
        });
    };
    
    // Unreachable code block
    _reachability.unreachableBlock = ^(Reachability *reach)
    {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            for (NSDictionary* observerDict in weakSelf.observers) {
                PLTConnectionMonitorBlock unreachable = [observerDict objectForKey:kKeyUnreachableBlock];
                if (unreachable) {
                    unreachable();
                }
            }
        });
    };
    
    [_reachability startNotifier];
}

#pragma mark - Memory Management

- (void)dealloc {
    [_reachability stopNotifier];
}

@end
