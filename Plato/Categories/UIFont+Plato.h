//
//  UIFont+Plato.h
//  Plato
//
//  Created by Marc Janga on 18/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Example define font sizes:
 typedef enum {
    PLTFontSizeSmall = 10,
    PLTFontSizeRegular = 14,
    PLTFontSizeBig = 20,
    PLTFontSizeAlmostHuge = 44,
    PLTFontSizeHuge = 60
 } PLTFontSize;
 
 */

@interface UIFont (Plato)

#pragma mark - Custom font definitions
/*
 
 Example:
 + (UIFont*)platoFontRegularWithSize:(CGFloat)size;
 + (UIFont*)platoFontHeavyWithSize:(CGFloat)size;
 + (UIFont*)platoFontLightWithSize:(CGFloat)size;
 
 */

@end
