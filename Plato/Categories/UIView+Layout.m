//
//  UIView+Layout.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "UIView+Layout.h"

@implementation UIView (Layout)

- (void)addFillingSubview:(UIView *)view {
    [self addSubview:view withFullWidth:YES fullHeight:YES withMargins:UIEdgeInsetsZero];
}

- (void)addFillingSubview:(UIView *)view withMargins:(UIEdgeInsets)insets {
    [self addSubview:view withFullWidth:YES fullHeight:YES withMargins:insets];
}

- (void)addFullWidthSubview:(UIView *)view {
    [self addSubview:view withFullWidth:YES fullHeight:NO withMargins:UIEdgeInsetsZero];
}

- (void)addFullHeightSubview:(UIView *)view {
    [self addSubview:view withFullWidth:NO fullHeight:YES withMargins:UIEdgeInsetsZero];
}

- (void)addSubview:(UIView *)view withFullWidth:(BOOL)fullWidth fullHeight:(BOOL)fullHeight withMargins:(UIEdgeInsets)margins {
    view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:view];
    
    NSMutableDictionary *metrics = [NSMutableDictionary new];
    metrics[@"marginTop"]   = @(margins.top);
    metrics[@"marginBot"]   = @(margins.bottom);
    metrics[@"marginLeft"]  = @(margins.left);
    metrics[@"marginRight"] = @(margins.right);
    
    NSDictionary *views = @{@"view":view};
    if(fullWidth) {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(marginLeft)-[view]-(marginRight)-|" options:0 metrics:metrics views:views]];
    }
    if(fullHeight) {
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(marginTop)-[view]-(marginBot)-|" options:0 metrics:metrics views:views]];
    }
}

@end
