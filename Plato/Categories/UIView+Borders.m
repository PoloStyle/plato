//
//  UIView+Borders.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "UIView+Borders.h"
#import <objc/runtime.h>

static char BORDER_BOTTOM;
static char BORDER_TOP;
static char BORDER_LEFT;
static char BORDER_RIGHT;

static char BORDER_VIEW_BOTTOM;
static char BORDER_VIEW_TOP;
static char BORDER_VIEW_LEFT;
static char BORDER_VIEW_RIGHT;

@implementation UIView (Borders)

@dynamic borderBottom, borderTop, borderRight, borderLeft, borderViewBottom, borderViewTop, borderViewLeft, borderViewRight;

#pragma mark - Public Border Layers -

-(void)addBorderWithColor:(UIColor *)color {
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = 1.0f;
}

- (void) addBorderWithColor:(UIColor*)color andWidth:(CGFloat)width atPosition:(enum BorderPosition)position {
    [self addBorderWithColor:color andWidth:width atPosition:position withOffSetFirst:0 andOffSetSecond:0];
}

- (void) addBorderWithColor:(UIColor*)color andWidth:(CGFloat)width atPosition:(enum BorderPosition)position withOffSetFirst:(CGFloat)offSetOne andOffSetSecond:(CGFloat)offSetTwo
{
    CGRect frame;
    
    switch (position) {
        case BorderPositionBottom:
            frame = CGRectMake(0 + offSetOne, self.frame.size.height-width, self.frame.size.width - (offSetOne + offSetTwo), width);
            [self.borderBottom removeFromSuperlayer];
            self.borderBottom = [self addBorderWithFrame:frame andColor:color];
            break;
        case BorderPositionRight:
            frame = CGRectMake(self.frame.size.width-width, 0 + offSetOne, width, self.frame.size.height - (offSetOne + offSetTwo));
            [self.borderRight removeFromSuperlayer];
            self.borderRight = [self addBorderWithFrame:frame andColor:color];
            break;
        case BorderPositionTop:
            frame = CGRectMake(0 + offSetOne, 0, self.frame.size.width - (offSetOne + offSetTwo), width);
            [self.borderTop removeFromSuperlayer];
            self.borderTop = [self addBorderWithFrame:frame andColor:color];
            break;
        case BorderPositionLeft:
            frame = CGRectMake(0, 0 + offSetOne, width, self.frame.size.height - (offSetOne + offSetTwo));
            [self.borderLeft removeFromSuperlayer];
            self.borderLeft = [self addBorderWithFrame:frame andColor:color];
            break;
    }
}

#pragma mark - Public Border Views -

- (void)addBorderViewWithColor:(UIColor*)color width:(CGFloat)width position:(enum BorderPosition)position
                    leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin {
    UIView *borderView = [UIView new];
    borderView.translatesAutoresizingMaskIntoConstraints = NO;
    borderView.backgroundColor = color;
    [self addSubview:borderView];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(borderView);
    NSDictionary *metrics = @{@"width"      : @(width),
                              @"leftMargin" : @(leftMargin),
                              @"rightMargin": @(rightMargin)};
    switch (position) {
        case BorderPositionBottom:
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-leftMargin-[borderView]-rightMargin-|"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[borderView(width)]|"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            self.borderViewBottom = borderView; break;
        case BorderPositionRight:
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[borderView(width)]|"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-leftMargin-[borderView]-rightMargin-|"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            self.borderViewRight = borderView;  break;
        case BorderPositionTop:
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-leftMargin-[borderView]-rightMargin-|"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[borderView(width)]"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            self.borderViewTop = borderView;    break;
        case BorderPositionLeft:
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[borderView(width)]"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-leftMargin-[borderView]-rightMargin-|"
                                                                         options:0 metrics:metrics views:viewsDictionary]];
            self.borderViewLeft = borderView;   break;
    }
}

#pragma mark - Private -

-(CALayer *)addBorderWithFrame:(CGRect)frame andColor:(UIColor*)color
{
    CALayer *border = [CALayer layer];
    border.frame = frame;
    [border setBackgroundColor:color.CGColor];
    [self.layer addSublayer:border];
    
    return border;
}

#pragma mark - Category Property Setters -

- (void) setBorderBottom:(CALayer *)borderBottom {
    objc_setAssociatedObject(self, &BORDER_BOTTOM, borderBottom, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) setBorderTop:(CALayer *)borderTop {
    objc_setAssociatedObject(self, &BORDER_TOP, borderTop, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) setBorderLeft:(CALayer *)borderLeft {
    objc_setAssociatedObject(self, &BORDER_LEFT, borderLeft, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) setBorderRight:(CALayer *)borderRight {
    objc_setAssociatedObject(self, &BORDER_RIGHT, borderRight, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setBorderViewBottom:(UIView *)borderViewBottom {
    objc_setAssociatedObject(self, &BORDER_VIEW_BOTTOM, borderViewBottom, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setBorderViewTop:(UIView *)borderViewTop {
    objc_setAssociatedObject(self, &BORDER_VIEW_TOP, borderViewTop, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setBorderViewLeft:(UIView *)borderViewLeft {
    objc_setAssociatedObject(self, &BORDER_VIEW_LEFT, borderViewLeft, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setBorderViewRight:(UIView *)borderViewRight {
    objc_setAssociatedObject(self, &BORDER_VIEW_RIGHT, borderViewRight, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Category Property Getters -

- (CALayer *) borderBottom {
    return objc_getAssociatedObject(self, &BORDER_BOTTOM);
}

- (CALayer *) borderTop {
    return objc_getAssociatedObject(self, &BORDER_TOP);
}

- (CALayer *) borderLeft {
    return objc_getAssociatedObject(self, &BORDER_LEFT);
}

- (CALayer *) borderRight {
    return objc_getAssociatedObject(self, &BORDER_RIGHT);
}

- (UIView *)borderViewBottom {
    return objc_getAssociatedObject(self, &BORDER_VIEW_BOTTOM);
}

- (UIView *)borderViewTop {
    return objc_getAssociatedObject(self, &BORDER_VIEW_TOP);
}

- (UIView *)borderViewLeft {
    return objc_getAssociatedObject(self, &BORDER_VIEW_LEFT);
}

- (UIView *)borderViewRight {
    return objc_getAssociatedObject(self, &BORDER_VIEW_RIGHT);
}

@end
