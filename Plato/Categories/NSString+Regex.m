//
//  NSString+Regex.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "NSString+Regex.h"

@implementation NSString (Regex)

- (BOOL)isValidYoutubeLink{
    
    return [self isValidRegularYoutubeLink] || [self isValidShortYoutubeLink];
}

- (BOOL)isValidShortYoutubeLink{
    
    NSString *shortYoutubeRegEx = @"^http(?:s)?:\\/\\/(?:www\\.)?youtu.be\\/.{11}$";
    NSPredicate *shortYoutubeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", shortYoutubeRegEx];
    return [shortYoutubeTest evaluateWithObject:self];
}

- (BOOL)isValidRegularYoutubeLink{
    
    NSString *youtubeRegEx = @"^http(?:s)?:\\/\\/(?:www\\.)?(?:m.)?youtube.com\\/watch\\?(?=[^?]*v=\\w+)(?:[^\\s?]+)?$";
    NSPredicate *youtubeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", youtubeRegEx];
    return [youtubeTest evaluateWithObject:self];
}

- (BOOL)isValidAlphaNumericString{
    
    NSString* regex = @"^(\\w|\\s|'|\")*$";
    NSPredicate* test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:self];
}

- (BOOL)isValidWeblink{
    
    NSString *regex = @"^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:self];
}

@end
