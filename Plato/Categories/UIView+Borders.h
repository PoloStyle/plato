//
//  UIView+Borders.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>

enum BorderPosition {
    BorderPositionTop,
    BorderPositionBottom,
    BorderPositionLeft,
    BorderPositionRight
};

@interface UIView (Borders)

// Border layers
@property (nonatomic, strong) CALayer *borderTop;
@property (nonatomic, strong) CALayer *borderBottom;
@property (nonatomic, strong) CALayer *borderLeft;
@property (nonatomic, strong) CALayer *borderRight;

- (void) addBorderWithColor:(UIColor*)color;
- (void) addBorderWithColor:(UIColor*)color andWidth:(CGFloat)width atPosition:(enum BorderPosition)position;
- (void) addBorderWithColor:(UIColor*)color andWidth:(CGFloat)width atPosition:(enum BorderPosition)position withOffSetFirst:(CGFloat)offSetOne andOffSetSecond:(CGFloat)offSetTwo;

// Border views
@property (nonatomic, strong) UIView *borderViewTop;
@property (nonatomic, strong) UIView *borderViewBottom;
@property (nonatomic, strong) UIView *borderViewLeft;
@property (nonatomic, strong) UIView *borderViewRight;

- (void)addBorderViewWithColor:(UIColor*)color width:(CGFloat)width position:(enum BorderPosition)position
                    leftMargin:(CGFloat)leftMargin rightMargin:(CGFloat)rightMargin;

@end
