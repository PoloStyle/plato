//
//  NSURL+Utils.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "NSURL+Utils.h"

@implementation NSURL (Utils)

-(NSString *)valueForParameter:(NSString *)parameterName {
    NSURLComponents * comp = [NSURLComponents componentsWithURL:self resolvingAgainstBaseURL:NO];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        //To remove once we drop support for iOS 7 devices
        NSString *queryString = comp.query;
        NSRange range = [queryString rangeOfString:[parameterName stringByAppendingString:@"="]];
        NSInteger nextParamSearchLocation = range.location + range.length;
        if(!range.length || nextParamSearchLocation >= queryString.length ){
            return nil;
        }
        
        NSRange nextParamSearchRange = NSMakeRange(nextParamSearchLocation, queryString.length - nextParamSearchLocation);
        NSRange nextParamStart = [queryString rangeOfString:@"&" options:0 range:nextParamSearchRange];
        if(nextParamStart.length) {
            return [queryString substringWithRange:NSMakeRange(nextParamSearchLocation, nextParamStart.location - nextParamSearchLocation)];
        } else {
            return [queryString substringFromIndex:nextParamSearchLocation];
        }
        
    } else {
        NSArray * queryItems = [comp.queryItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name = %@", parameterName]];
        NSURLQueryItem * queryItem = [queryItems firstObject];
        return queryItem.value;
    }
}

// Url encode mehod obtained from
//   http://madebymany.com/blog/url-encoding-an-nsstring-on-ios
//   look this web for a good explanation
+ (NSString *)urlEncode:(NSString *)unencodedString {
    NSString *encodedString =
    (NSString *)CFBridgingRelease(
                                  CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                          (CFStringRef)unencodedString,
                                                                          NULL,
                                                                          (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                          kCFStringEncodingUTF8));
    return encodedString;
}

+ (NSDictionary *)parseQueryParamsFromURL:(NSURL *)url {
    NSArray *urlComponents = [url.query componentsSeparatedByString:@"&"];
    NSMutableDictionary *queryParams = [NSMutableDictionary dictionary];
    NSCharacterSet *whiteSpaces = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    for (NSString *keyValuePair in urlComponents) {
        NSArray  *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key   = [[pairComponents objectAtIndex:0] stringByTrimmingCharactersInSet:whiteSpaces];
        NSString *value = [[pairComponents objectAtIndex:1] stringByTrimmingCharactersInSet:whiteSpaces];
        queryParams[key] = value;
    }
    return queryParams;
}

@end
