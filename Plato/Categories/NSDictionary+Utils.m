//
//  NSDictionary+Utils.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "NSDictionary+Utils.h"

@implementation NSDictionary (Utils)

#pragma mark - Object checks -

- (id)objectOrNilForKey:(id)aKey {
    id obj = [self objectForKey:aKey];
    return ([obj isKindOfClass:[NSNull class]]) ? nil : obj;
}

- (NSString *)objectOrBlankStringForKey:(id)aKey {
    id obj = [self objectForKey:aKey];
    return ([obj isKindOfClass:[NSNull class]]) ? @"" : obj;
}

#pragma mark - Json -

- (NSData *)jsonData {
    
    if ( !self ) {
        return nil;
    }
    
    NSData *customData = nil;
    NSError *jsonError = nil;
    
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&jsonError];
    if (!jsonError) {
        
        NSString *bodyString = [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding];
        customData = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    return customData;
}

@end
