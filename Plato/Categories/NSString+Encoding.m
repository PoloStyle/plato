//
//  NSString+Encoding.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "NSString+Encoding.h"
#import "NSData+Encoding.h"
#import <CommonCrypto/CommonCrypto.h>

static NSString * const kSmartOnlineAESKey = @"805aa7c5790aa31bdbaf8dda439269f2";

@implementation NSString (Encoding)

#pragma mark - Encoding -

- (NSString *)sha1 {
    const char *cStr = [self UTF8String];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1( cStr, (CC_LONG)strlen(cStr), result );
    NSString *hash = [NSString stringWithFormat:
                      @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                      result[0],  result[1],  result[2],  result[3],
                      result[4],  result[5],  result[6],  result[7],
                      result[8],  result[9],  result[10], result[11],
                      result[12], result[13], result[14], result[15],
                      result[16], result[17], result[18], result[19]
                      ];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return hash;
}

- (NSString *)md5 {
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    NSString *hash = [NSString stringWithFormat:
                      @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                      result[0],  result[1],  result[2],  result[3],
                      result[4],  result[5],  result[6],  result[7],
                      result[8],  result[9],  result[10], result[11],
                      result[12], result[13], result[14], result[15]
                      ];
    
    return hash;
}

- (NSString *)cipherAES {
    NSData *plain = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipher = [self aesOperation:kCCEncrypt OnData:plain];
    return [cipher hexadecimalString];
}

- (NSString *)decipherAES {
    NSData *cipher = [NSData dataWithHexadecimalEncoding:self];
    NSData *plain = [self aesOperation:kCCDecrypt OnData:cipher];
    return [[NSString alloc] initWithData:plain encoding:NSUTF8StringEncoding];
}

- (NSData *)aesOperation:(CCOperation)op OnData:(NSData *)data {
    NSData *outData = nil;
    
    // Data in parameters
    NSData *myData = [NSData dataWithHexadecimalEncoding:kSmartOnlineAESKey];
    const void *key = myData.bytes;
    const void *dataIn = data.bytes;
    size_t dataInLength = data.length;
    // Data out parameters
    size_t outMoved = 0;
    
    size_t bufferSize = dataInLength + kCCBlockSizeAES128;
    
    // Init out buffer
    unsigned char outBuffer[bufferSize];
    memset(outBuffer, 0, bufferSize);
    CCCryptorStatus status = -1;
    
    status = CCCrypt(op, kCCAlgorithmAES128, kCCOptionPKCS7Padding | kCCOptionECBMode, key, kCCKeySizeAES128, NULL,
                     dataIn, dataInLength, &outBuffer, bufferSize, &outMoved);
    
    if (status == kCCSuccess) {
        outData = [NSData dataWithBytes:outBuffer length:outMoved];
        
    } else if (status == kCCBufferTooSmall) {
        // Resize the out buffer
        size_t newsSize = outMoved;
        void *dynOutBuffer = malloc(newsSize);
        memset(dynOutBuffer, 0, newsSize);
        outMoved = 0;
        
        status = CCCrypt(op, kCCAlgorithmAES128, kCCOptionPKCS7Padding | kCCOptionECBMode, key, kCCKeySizeAES128, NULL,
                         dataIn, dataInLength, &outBuffer, bufferSize, &outMoved);
        
        if (status == kCCSuccess) {
            outData = [NSData dataWithBytes:outBuffer length:outMoved];
        }
    }
    
    return outData;
}

+ (NSString *)generateUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

#pragma mark - Conversions -

- (NSArray *)arrayFromCommaSeparateString  {
    NSArray *components = [self componentsSeparatedByString:@","];
    NSMutableArray *mutableArray = [NSMutableArray array];
    for (NSString *component in components) {
        NSInteger number = [component integerValue];
        [mutableArray addObject:[NSNumber numberWithInt:(int)number]];
    }
    return [mutableArray copy];
}

#pragma mark - Formatting -

- (NSString *)stringByStrippingHtml {
    NSRange range;
    NSString *string = [self copy];
    while ((range = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:range withString:@""];
    return string;
}

- (NSString *)URLEscapedString {
    CFStringRef escapedString = CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)self, NULL, CFSTR("!*'()/;:@;=+$,?%#[]&"), kCFStringEncodingUTF8);
    return  (__bridge_transfer NSString *)escapedString;
}

+ (NSString *)bytesToHumanReadable:(long long)bytes {
    // Convert research size to appropriate units
    double showSize = (double)bytes;
    
    NSArray  *units = @[@"B", @"KB", @"MB", @"GB"];
    NSString *unit  = [units lastObject];
    
    for (NSString *currentUnit in units) {
        if ((showSize / 1024.0f) < 1.0f) {
            unit = currentUnit;
            break;
        }
        else {
            showSize = showSize / 1024.0f;
        }
    }
    
    return [NSString stringWithFormat:@"%.1f %@", showSize, unit];
}

+ (NSString *)parseRFC3339Date:(NSDate *)date {
    
    return [self dateStringForDate:date andFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    
}

+ (NSString *)parseRFC3339DateWith2Miliseconds:(NSDate *)date {
    
    return [self dateStringForDate:date andFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
    
}

+ (NSString *)stringWithDate:(NSDate *)date usingDateFormatter:(NSDateFormatter *)dateFormatter {
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)dateStringForDate:(NSDate *)date andFormat:(NSString *)dateFormat {
    
    static NSDateFormatter *formatter;
    
    formatter = [[NSDateFormatter alloc] init];
    
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:dateFormat];
    
    NSString *dateString = [formatter stringFromDate:date];
    if ( dateString ) {
        return dateString;
    } else {
        return @"";
    }
}

#pragma mark - Match check -

- (BOOL)isMatchingRegularExpression:(NSRegularExpression *)regExpression {
    NSRange firstMatch = [regExpression rangeOfFirstMatchInString:self
                                                          options:0
                                                            range:NSMakeRange(0, self.length)];
    return firstMatch.location != NSNotFound;
}

@end
