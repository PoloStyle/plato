//
//  UIAlertView+Blocks.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Blocks)

typedef void (^SelectBlock)(NSInteger buttonIndex);
typedef void (^CancelBlock)();

+ (UIAlertView *) alertViewWithTitle:(NSString *)title
                             message:(NSString *)message
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                   otherButtonTitles:(NSArray *)otherButtons
                            onSelect:(SelectBlock)selected
                            onCancel:(CancelBlock)cancelled;

@end
