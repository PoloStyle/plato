//
//  NSString+Encoding.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encoding)

// Encoding
- (NSString *)sha1;
- (NSString *)md5;
- (NSString *)cipherAES;
- (NSString *)decipherAES;

+ (NSString *)generateUUID;

// Conversions
- (NSArray *)arrayFromCommaSeparateString;

// Fromatting
- (NSString *)URLEscapedString;
- (NSString *)stringByStrippingHtml;

+ (NSString*)bytesToHumanReadable:(long long)bytes;

+ (NSString *)parseRFC3339Date:(NSDate *)date;
+ (NSString *)parseRFC3339DateWith2Miliseconds:(NSDate *)date;

// Check
- (BOOL)isMatchingRegularExpression:(NSRegularExpression *)regExpression;

@end
