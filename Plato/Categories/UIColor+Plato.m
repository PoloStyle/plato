//
//  UIColor+Plato.m
//  Plato
//
//  Created by Marc Janga on 18/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "UIColor+Plato.h"

@implementation UIColor (Plato)

#pragma mark - Custom color definitions

/*
    Example:
    + (UIColor *)platoBlueColor {
        return [UIColor colorWithHexString:@"009eb4" andAlpha:1.0f];
    }

    + (UIColor *)platoRedColor {
        return [UIColor colorWithHexString:@"dd1d21" andAlpha:1.0f];
    }
  */

@end
