//
//  UIColor+Utils.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

// Colors
+ (UIColor *)randomColor;
+ (UIColor *)colorWithHexValue:(uint)hexValue;
+ (UIColor *)colorWithHexValue:(uint)hexValue andAlpha:(float)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(float)alpha;

// Color image
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

@end
