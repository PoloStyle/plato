//
//  UIDevice+Info.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <sys/utsname.h>
#import "UIDevice+Info.h"

static NSString *const kSimulatorModel = @"Simulator";
static NSString *const kiPod = @"iPod Touch 1";
static NSString *const kiPod2 = @"iPod Touch 2";
static NSString *const kiPod3 = @"iPod Touch 3";
static NSString *const kiPod4 = @"iPod Touch 4";
static NSString *const kiPod5 = @"iPod Touch 5";
static NSString *const kiPhone = @"iPhone";
static NSString *const kiPhone3G = @"iPhone 3G";
static NSString *const kiPhone3GS = @"iPhone 3GS";
static NSString *const kiPhone4 = @"iPhone 4";
static NSString *const kiPhone4S = @"iPhone 4S";
static NSString *const kiPhone5 = @"iPhone 5";
static NSString *const kiPhone5S = @"iPhone 5S";
static NSString *const kiPhone5C = @"iPhone 5C";
static NSString *const kiPad = @"iPad";
static NSString *const kiPad2 = @"iPad 2";
static NSString *const kiPad3 = @"iPad 3";
static NSString *const kiPad4 = @"iPad 4";
static NSString *const kiPadMini = @"iPad mini";
static NSString *const kiPadMini2 = @"iPad mini 2";
static NSString *const kiPadAir = @"iPad air";
static NSString *const kUnknown = @"Unknown";

@implementation UIDevice (Info)

- (NSString *)detailedModel {
    
    NSDictionary *modelDictionary = @{@"i386":kSimulatorModel, @"iPod1,1":kiPod,
                                      @"iPod2,1":kiPod2, @"iPod3,1":kiPod3,
                                      @"iPod4,1":kiPod4, @"iPod5,1":kiPod5,
                                      @"iPhone1,1":kiPhone, @"iPhone1,2":kiPhone3G,
                                      @"iPhone2,1":kiPhone3GS, @"iPhone3,1":kiPhone4,
                                      @"iPhone4,1":kiPhone4S, @"iPhone5,1":kiPhone5,
                                      @"iPhone5,2":kiPhone5, @"iPhone5,3":kiPhone5C,
                                      @"iPhone5,4":kiPhone5C,@"iPhone6,1":kiPhone5S,
                                      @"iPhone6,2":kiPhone5S,
                                      @"iPad1,1":kiPad, @"iPad2,1":kiPad2,
                                      @"iPad3,1":kiPad3, @"iPad3,4":kiPad4,
                                      @"iPad2,5":kiPadMini, @"iPad4,4":kiPadMini2,
                                      @"iPad4,5":kiPadMini2, @"iPad4,1":kiPadAir,
                                      @"iPad4,2":kiPadAir };
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *modelString = [NSString stringWithCString:systemInfo.machine
                                               encoding:NSUTF8StringEncoding];
    modelString = [modelDictionary objectForKey:modelString];
    if(!modelString){
        modelString = kUnknown;
    }
    return modelString;
}

@end
