//
//  NSData+Encoding.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Encoding)

// Conversions
- (NSString *)hexadecimalString;
+ (NSData *)dataWithHexadecimalEncoding:(NSString *)hexString;

// Encryption
- (NSData*)AES256EncryptWithKey:(NSString*)key;
- (NSData*)AES256DecryptWithKey:(NSString*)key;
- (NSString *)StringAES256DecryptWithKey:(NSString *)key;

- (NSString *)base64;
+ (NSData *)base64DataFromString: (NSString *)string;


@end
