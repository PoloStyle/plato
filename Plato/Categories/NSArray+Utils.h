//
//  NSArray+Utils.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Utils)

- (NSString *)arrayToCommaSeparateString;

@end
