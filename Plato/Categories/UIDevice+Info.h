//
//  UIDevice+Info.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IOS8_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0)

#define IS_IOS7 ([[UIDevice currentDevice].systemVersion floatValue] < 8.0 && [[UIDevice currentDevice].systemVersion floatValue] >= 7.0)

@interface UIDevice (Info)

- (NSString *)detailedModel;

@end
