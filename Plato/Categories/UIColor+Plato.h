//
//  UIColor+Plato.h
//  Plato
//
//  Created by Marc Janga on 18/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Utils.h"

@interface UIColor (Plato)

#pragma mark - Custom color definitions
/*
 
 Example:
 + (UIColor *)platoBlueColor; // Custom Blue for Plato project
 + (UIColor *)platoRedColor; // Custom Red for Plato project
 
 */

@end
