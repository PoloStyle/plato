//
//  NSDictionary+Utils.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utils)

- (NSData *)jsonData;

- (id)objectOrNilForKey:(id)aKey;
- (NSString *)objectOrBlankStringForKey:(id)aKey;

@end
