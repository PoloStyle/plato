//
//  UIFont+Plato.m
//  Plato
//
//  Created by Marc Janga on 18/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "UIFont+Plato.h"

@implementation UIFont (Plato)

#pragma mark - Custom font definitions
/*
    Example:
    + (UIFont*)platoFontRegularWithSize:(CGFloat)size {
        return [UIFont fontWithName:@"HelveticaNeue" size:size];
    }

    + (UIFont*)platoFontHeavyWithSize:(CGFloat)size {
        return [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:size];
    }

    + (UIFont*)platoFontLightWithSize:(CGFloat)size {
        return [UIFont fontWithName:@"HelveticaNeue-Light" size:size];
    }
 */

@end
