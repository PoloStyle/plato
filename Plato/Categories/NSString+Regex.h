//
//  NSString+Regex.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Regex)

- (BOOL)isValidWeblink;

- (BOOL)isValidYoutubeLink;
- (BOOL)isValidShortYoutubeLink;
- (BOOL)isValidRegularYoutubeLink;

- (BOOL)isValidAlphaNumericString;

@end
