//
//  UIAlertView+Blocks.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "UIAlertView+Blocks.h"

@implementation UIAlertView (Blocks)

static SelectBlock  _selectBlock;
static CancelBlock  _cancelBlock;

+ (UIAlertView *) alertViewWithTitle:(NSString *)title
                             message:(NSString *)message
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                   otherButtonTitles:(NSArray *)otherButtons
                            onSelect:(SelectBlock)selected
                            onCancel:(CancelBlock)cancelled
{
    
    _selectBlock = [selected copy];
    _cancelBlock = [cancelled copy];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
    
    for (NSString *buttonTitle in otherButtons) {
        [alert addButtonWithTitle:buttonTitle];
    }
    
    if (![NSThread isMainThread ])
    {
        dispatch_sync(dispatch_get_main_queue(), ^{
            [alert show];
        });
    }else
        [alert show];
    
    
    return alert;
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        if (!_cancelBlock)
            return;
        _cancelBlock();
    } else {
        if (!_selectBlock)
            return;
        _selectBlock(buttonIndex - 1);
    }
    
    //2 alerts in a row were keeping this blocks and executnig them. Which is not nice :D
    _cancelBlock = nil;
    _selectBlock = nil;
}

@end
