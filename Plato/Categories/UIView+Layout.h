//
//  UIView+Layout.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Layout)

- (void)addFillingSubview:(UIView *)view withMargins:(UIEdgeInsets)insets;
- (void)addFillingSubview:(UIView *)view;
- (void)addFullWidthSubview:(UIView *)view;
- (void)addFullHeightSubview:(UIView *)view;

@end
