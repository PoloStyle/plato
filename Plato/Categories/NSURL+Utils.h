//
//  NSURL+Utils.h
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Utils)

-(NSString *)valueForParameter:(NSString *)parameterName;

+ (NSString*)urlEncode:(NSString*)unencodedString;
+ (NSDictionary *)parseQueryParamsFromURL:(NSURL *)url;

@end
