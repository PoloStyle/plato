//
//  NSArray+Utils.m
//  Plato
//
//  Created by Marc Janga on 14/08/15.
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "NSArray+Utils.h"

@implementation NSArray (Utils)

- (NSString *)arrayToCommaSeparateString {
    NSMutableString *string = [NSMutableString string];
    BOOL isFirstIteration = YES;
    for (NSNumber *item in self) {
        if (isFirstIteration) {
            isFirstIteration = NO;
            [string appendFormat:@"%d", (int)item.integerValue];
        } else {
            [string appendFormat:@",%d", (int)item.integerValue];
        }
    }
    return [string copy];
}

@end
