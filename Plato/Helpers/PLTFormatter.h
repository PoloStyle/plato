//
//  PLTFormatter.h
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PLTFormatter : NSObject

#pragma mark - Locales

+ (NSLocale *)locale;

#pragma mark - Date formatters

+ (NSDateFormatter *)standarDateFormatter;
+ (NSDateFormatter *)dateDayLongMonthYearFormatter;
+ (NSDateFormatter *)dateDayMonthYearFormatter;
+ (NSDateFormatter *)dateHourMinuteFormatter;
+ (NSNumberFormatter *)decimalNumberFormatter;
+ (NSNumberFormatter *)decimalCRTNumberFormatter;

#pragma mark - Number formatters

+ (NSNumberFormatter*)currencyFormatterForSymbol:(NSString*)symbol;

+ (NSNumberFormatter *)defaultNumberFormatter;
+ (NSNumberFormatter *)percentFormatter;
+ (NSNumberFormatter *)percentFormatterWithFractionDigits:(NSInteger)fractionDigits;
+ (NSNumberFormatter *)percentSignedFormatter;

@end
