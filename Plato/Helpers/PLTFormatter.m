//
//  PLTFormatter.m
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "PLTFormatter.h"

@implementation PLTFormatter

#pragma mark - Locales -

+ (NSLocale *)locale {
    static NSLocale *locale = nil;
    
    if (locale == nil) {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    }
    
    return [locale copy];
}

#pragma mark - Date formatters -

+ (NSDateFormatter *)standarDateFormatter {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setLocale:[self locale]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return formatter;
}

+ (NSDateFormatter *)dateDayLongMonthYearFormatter {
    NSDateFormatter *normalDateFormatter = [NSDateFormatter new];
    normalDateFormatter.locale = [self locale];
    normalDateFormatter.dateFormat = @"dd MMMM yyyy";
    
    return normalDateFormatter;
}

+ (NSDateFormatter *)dateDayMonthYearFormatter {
    NSDateFormatter *normalDateFormatter = [NSDateFormatter new];
    normalDateFormatter.locale = [self locale];
    normalDateFormatter.dateFormat = @"dd MM yyyy";
    
    return normalDateFormatter;
}

+ (NSDateFormatter *)dateHourMinuteFormatter {
    NSDateFormatter *normalDateFormatter = [NSDateFormatter new];
    normalDateFormatter.locale = [self locale];
    normalDateFormatter.dateFormat = @"hh:mm";
    
    return normalDateFormatter;
}

#pragma mark - Number formatters -

+ (NSNumberFormatter*)currencyFormatterForSymbol:(NSString*)symbol{
    
    NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
    currencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    currencyFormatter.locale = [self locale];
    currencyFormatter.roundingMode = NSNumberFormatterRoundHalfUp;
    
    currencyFormatter.currencySymbol = [NSString stringWithFormat:@"%@ ",symbol];
    currencyFormatter.negativePrefix = [NSString stringWithFormat:@"- %@ ",symbol];
    currencyFormatter.negativeSuffix = @"";
    
    return currencyFormatter;
}

+ (NSNumberFormatter *)defaultNumberFormatter {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [self locale];
    numberFormatter.roundingMode = NSNumberFormatterRoundHalfUp;
    numberFormatter.minimumFractionDigits = 2;
    numberFormatter.maximumFractionDigits = 2;
    numberFormatter.groupingSeparator = @",";
    
    return numberFormatter;
}

+ (NSNumberFormatter *)decimalCRTNumberFormatter {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [self locale];
    numberFormatter.minimumFractionDigits = 0;
    numberFormatter.maximumFractionDigits = 4;
    numberFormatter.decimalSeparator = @".";
    numberFormatter.groupingSeparator = @",";
    
    return numberFormatter;
}

+ (NSNumberFormatter *)decimalNumberFormatter {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [self locale];
    numberFormatter.minimumFractionDigits = 0;
    numberFormatter.maximumFractionDigits = 2;
    numberFormatter.decimalSeparator = @".";
    numberFormatter.groupingSeparator = @",";
    
    return numberFormatter;
}

+ (NSNumberFormatter *)percentFormatter {
    NSNumberFormatter *percentFormatter = [NSNumberFormatter new];
    percentFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    percentFormatter.negativePrefix = @"-";
    percentFormatter.positiveSuffix = @"%";
    percentFormatter.negativeSuffix = @"%";
    percentFormatter.locale = [self locale];
    percentFormatter.roundingMode = NSNumberFormatterRoundHalfUp;
    [percentFormatter setMinimumFractionDigits:1];
    [percentFormatter setMaximumFractionDigits:2];
    
    return percentFormatter;
}

+ (NSNumberFormatter *)percentFormatterWithFractionDigits:(NSInteger)fractionDigits {
    NSNumberFormatter *percentFormatter = [[self percentFormatter] copy];
    [percentFormatter setMinimumFractionDigits:fractionDigits];
    [percentFormatter setMaximumFractionDigits:fractionDigits];
    
    return percentFormatter;
}

+ (NSNumberFormatter *)percentSignedFormatter {
    NSNumberFormatter *percentFormatter = [NSNumberFormatter new];
    percentFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    percentFormatter.negativePrefix = @"-";
    percentFormatter.positivePrefix = @"+";
    percentFormatter.positiveSuffix = @"%";
    percentFormatter.negativeSuffix = @"%";
    percentFormatter.locale = [self locale];
    percentFormatter.roundingMode = NSNumberFormatterRoundHalfUp;
    [percentFormatter setMinimumFractionDigits:1];
    [percentFormatter setMaximumFractionDigits:2];
    
    return percentFormatter;
}

@end
