
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 6
#define COCOAPODS_VERSION_PATCH_AFNetworking 0

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 0

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 0

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 0

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 0

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 0

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 0

// Asterism
#define COCOAPODS_POD_AVAILABLE_Asterism
#define COCOAPODS_VERSION_MAJOR_Asterism 1
#define COCOAPODS_VERSION_MINOR_Asterism 0
#define COCOAPODS_VERSION_PATCH_Asterism 0

// DZNEmptyDataSet
#define COCOAPODS_POD_AVAILABLE_DZNEmptyDataSet
#define COCOAPODS_VERSION_MAJOR_DZNEmptyDataSet 1
#define COCOAPODS_VERSION_MINOR_DZNEmptyDataSet 7
#define COCOAPODS_VERSION_PATCH_DZNEmptyDataSet 1

// DateTools
#define COCOAPODS_POD_AVAILABLE_DateTools
#define COCOAPODS_VERSION_MAJOR_DateTools 1
#define COCOAPODS_VERSION_MINOR_DateTools 7
#define COCOAPODS_VERSION_PATCH_DateTools 0

// FormatterKit
#define COCOAPODS_POD_AVAILABLE_FormatterKit
#define COCOAPODS_VERSION_MAJOR_FormatterKit 1
#define COCOAPODS_VERSION_MINOR_FormatterKit 8
#define COCOAPODS_VERSION_PATCH_FormatterKit 0

// FormatterKit/AddressFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_AddressFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_AddressFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_AddressFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_AddressFormatter 0

// FormatterKit/ArrayFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_ArrayFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_ArrayFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_ArrayFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_ArrayFormatter 0

// FormatterKit/ColorFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_ColorFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_ColorFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_ColorFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_ColorFormatter 0

// FormatterKit/LocationFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_LocationFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_LocationFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_LocationFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_LocationFormatter 0

// FormatterKit/NameFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_NameFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_NameFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_NameFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_NameFormatter 0

// FormatterKit/OrdinalNumberFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_OrdinalNumberFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_OrdinalNumberFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_OrdinalNumberFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_OrdinalNumberFormatter 0

// FormatterKit/TimeIntervalFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_TimeIntervalFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_TimeIntervalFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_TimeIntervalFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_TimeIntervalFormatter 0

// FormatterKit/URLRequestFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_URLRequestFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_URLRequestFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_URLRequestFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_URLRequestFormatter 0

// FormatterKit/UnitOfInformationFormatter
#define COCOAPODS_POD_AVAILABLE_FormatterKit_UnitOfInformationFormatter
#define COCOAPODS_VERSION_MAJOR_FormatterKit_UnitOfInformationFormatter 1
#define COCOAPODS_VERSION_MINOR_FormatterKit_UnitOfInformationFormatter 8
#define COCOAPODS_VERSION_PATCH_FormatterKit_UnitOfInformationFormatter 0

// GoogleAnalytics
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics 13
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics 0

// IHKeyboardAvoiding
#define COCOAPODS_POD_AVAILABLE_IHKeyboardAvoiding
#define COCOAPODS_VERSION_MAJOR_IHKeyboardAvoiding 2
#define COCOAPODS_VERSION_MINOR_IHKeyboardAvoiding 4
#define COCOAPODS_VERSION_PATCH_IHKeyboardAvoiding 0

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 2
#define COCOAPODS_VERSION_PATCH_Reachability 0

// SZTextView
#define COCOAPODS_POD_AVAILABLE_SZTextView
#define COCOAPODS_VERSION_MAJOR_SZTextView 1
#define COCOAPODS_VERSION_MINOR_SZTextView 2
#define COCOAPODS_VERSION_PATCH_SZTextView 1

