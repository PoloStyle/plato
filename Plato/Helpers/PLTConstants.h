//
//  PLTConstants.h
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#ifndef Plato_PLTConstants_h
#define Plato_PLTConstants_h

// Macros to get the kind of device
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_568 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)

// Macros to get the orientation of device
#define IS_PORTRAIT (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
#define IS_LANDSCAPE (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))

// iOS Version check
#define IS_IOS6_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)
#define IS_IOS7_AND_UP ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)

#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00)

// Macro to manage NSNull as nil
#define NULL_TO_NIL(obj) ([obj isKindOfClass:NSNull.class] ? nil : obj)

#endif
