//
//  PLTLogger.h
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#ifndef Plato_PLTLogger_h
#define Plato_PLTLogger_h

#define XCODE_COLORS_ESCAPE @"\033["

#define XCODE_COLORS_RESET_FG  XCODE_COLORS_ESCAPE @"fg;" // Clear any foreground color
#define XCODE_COLORS_RESET_BG  XCODE_COLORS_ESCAPE @"bg;" // Clear any background color
#define XCODE_COLORS_RESET     XCODE_COLORS_ESCAPE @";"   // Clear any foreground or background color

// Debug only log
#ifdef DEBUG
    #define DLog(fmt, ...) NSLog((@"%s [L:%d] " fmt), __func__, __LINE__, ##__VA_ARGS__);

    /** Below macros can be used used in conjunction with the XcodeColors Xcode plugin
     See http://djromero.wuonm.com/xcodecolors-xcode-plugin/ for install instructions or
     install from Alcatraz (http://alcatraz.io)
     **/

    // DLogError = white on red
    #define DLogError(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg220,0,0;" @"ERROR: %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);
    // DLogInfoBlue = white on blue
    #define DLogInfoBlue(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg0,0,255;" @"INFO: %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);
    // DLogInfoOrange = white on orange
    #define DLogInfoOrange(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg255,102,51;" @"INFO %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);
    // DLogInfoGreen = white on green
    #define DLogInfoGreen(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg0,204,51;" @"INFO %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);

    #define DSLog(fmt, ...) NSLog(fmt, ##__VA_ARGS__);
#else
    #define DLog(...)
    #define DLogError(...)
    #define DLogInfoBlue(...)
    #define DLogInfoOrange(...)
    #define DLogInfoGreen(...)
    #define DSLog(...)
#endif

// ALog always displays output
#define ALog(fmt, ...) NSLog((@"%s [L:%d] " fmt), __func__, __LINE__, ##__VA_ARGS__);
#define ASLog(fmt, ...) NSLog(fmt, ##__VA_ARGS__);

//Verbose log
#ifdef VERBOSE
    #define VLog(fmt, ...) NSLog((@"%s [L:%d] " fmt), __func__, __LINE__, ##__VA_ARGS__);

    /** Below macros can be used used in conjunction with the XcodeColors Xcode plugin
     See http://djromero.wuonm.com/xcodecolors-xcode-plugin/ for install instructions or
     install from Alcatraz (http://alcatraz.io)
     **/

    // VLogError = white on red
    #define VLogError(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg220,0,0;" @"ERROR: %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);
    // VLogInfoBlue = white on blue
    #define VLogInfoBlue(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg0,0,255;" @"INFO: %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);
    // VLogInfoOrange = white on orange
    #define VLogInfoOrange(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg255,102,51;" @"INFO %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);
    // VLogInfoGreen = white on green
    #define VLogInfoGreen(fmt, ...) NSLog((XCODE_COLORS_ESCAPE @"fg255,255,255;" XCODE_COLORS_ESCAPE @"bg0,204,51;" @"INFO %s [L:%d] " fmt XCODE_COLORS_RESET), __func__, __LINE__, ##__VA_ARGS__);

    #define VSLog(fmt, ...) NSLog(fmt, ##__VA_ARGS__);
#else
    #define VLog(...)
    #define VLogError(...)
    #define VLogInfoBlue(...)
    #define VLogInfoOrange(...)
    #define VLogInfoGreen(...)
    #define VSLog(...)
#endif

#endif
