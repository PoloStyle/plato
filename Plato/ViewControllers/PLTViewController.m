//
//  PLTViewController.m
//  Plato
//
//  Created by Marc Janga on 8/18/15
//  Copyright (c) 2015 Polonius. All rights reserved.
//

#import "PLTViewController.h"

@interface PLTViewController ()

@end

@implementation PLTViewController

#pragma mark - View Lifecycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self addCustomConstraints];
}

#pragma mark - Constraints -

- (void) addCustomConstraints {
    UILabel *plato = [UILabel new];
    plato.text = @"Plato";
    plato.textColor = [UIColor colorWithRed:239/255.0f green:92/255.0f blue:62/255.0f alpha:1.0f];
    plato.font = [UIFont systemFontOfSize:36.0f weight:0.5f];
    plato.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:plato];
    
    UILabel *served = [UILabel new];
    served.textColor = [UIColor blackColor];
    served.text = @"Your boilerplate is served!";
    served.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:served];
    
    // Explicit reference to superview so center autolayout works
    NSDictionary *viewsDict = @{@"plato":plato, @"served":served, @"superview":self.view};
    
    // Horizontal center
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[superview]-(<=1)-[plato]" options:NSLayoutFormatAlignAllCenterX metrics:nil views:viewsDict]];
    // Vertical center
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[superview]-(<=1)-[plato]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:viewsDict]];
    
    
    // Horizontal center and achor to bottom
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[served]-20-|" options:0 metrics:nil views:viewsDict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:served
                                 attribute:NSLayoutAttributeCenterX
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.view
                                 attribute:NSLayoutAttributeCenterX
                                multiplier:1.f constant:0.f]];
}

#pragma mark - Memory Management -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
