# ![icon](Plato/Resources/Images.xcassets/AppIcon.appiconset/Icon-60@2x.png) Plato

A featurefull iOS boilerplate.

## What is it?
We want to hit the ground running. Xcode and the iOS ecosystem don't make that easy. Enter Plato.  
After running one simple command, you get a ready-to-build universal iOS application, [full of delights](#whats-included).


## Can't wait?
Change to your projects directory, run this line in your terminal, replace username with your bitbucket username  
and follow the prompts:

```sh
ruby -e "$(curl -fsSL -u username https://bitbucket.org/PoloStyle/plato/raw/976852b86622a560ff86c6e42c5ddff18b692479/tiramisu)"
```

Of course, if you're wary of running random scripts (legit!), please read [tiramisu](tiramisu). At a high level, the  
script creates a local git repository with Plato as a remote named "bootstrap", tweaks filenames and  
contents as per your input, and grabs third-party code from Cocoapods.

(Tiramisu is Italian for "pick me up". Bootstrap... pick me up... get it?!? :dancer:).  
(Plato is Spanish for "plate". As in boiler..plate! :curry: Alright I'll stop now :sweat_smile:).


## Details and Requirements
The bootstrap assumes:

* You are using Xcode 5 or later.
* You have version 0.34.1 or later of the [CocoaPods gem](http://cocoapods.org/#install) installed.
* You are on OS 10.9 or later
* You are targetting iOS 7.0, at minimum (and thus will be compiling against at least the iOS 7.0 SDK).
    * As of [July 2014](https://developer.apple.com/support/appstore/), iOS 7 has an 90% adoption rate.

**Want to use Swift?** Plato won't stop you, but note that CocoaPods support for third-party Swift   
projects is still pending. 


## What's Included?
Plato aims to set you up with all you need to write a beautiful, maintainable, well-tested app. All the   
default pods are optional; feel free to pick and choose as needed for your project (though you will  
probably want most of them).

### Foundation
* A well-chosen class prefix is enforced
* A local git repository for the application is created (and committed to a few times through the  
initialization process).
* A sane `.gitignore` file is included.
* Sensible defaults for build options, warnings, and the like.
    * There are separate QA, stage, and production schemes, and corresponding   
    preprocessor macros. No more fiddling with variables here and there to switch your target  
    environment.
* Automatic ways to easily distinguish between builds of the app:
    * Development builds have their bundle id suffixed with ".qa" or ".stage" so that  
    they can co-exist on devices with other builds.
    * Development builds' icons are badged with a 🅠 for QA environments, an 🅢 for staging  
    environments and a 🅟 for production environments. The bundle names (but not the display names)  
    are also changed to easily distinguish them in places where it may otherwise be difficult.
* [CocoaPods](http://cocoapods.org) are integrated from the get-go.
* A barebones settings bundle is included with an "Acknowledgements" section that includes  
licenses for all your pods. It's automatically updated after each `pod install`.

### Logging
* A logger files is included with macros for debug and production logging, along with XcodeColors support for debug logs.  
[XcodeColors](https://github.com/robbiehanson/XcodeColors) can be installed through [Alcatraz](http://alcatraz.io/) and works nicely with the custom logger macros.

### Utility Belt
* [AFNetworking](https://github.com/AFNetworking/AFNetworking) A delightful networking library
* [FormatterKit](https://github.com/mattt/FormatterKit), for all your string-formatting needs.
* [Asterism](https://github.com/robb/Asterism), a fast, simple and flexible library for manipulating collections.
* [DateTools](https://github.com/MatthewYork/DateTools), Datetime heavy lifting.
* [Reachability](https://github.com/tonymillion/Reachability), a fast, simple and flexible library for manipulating collections.
* [GoogleAnalytics](http://www.google.com/analytics/), Most popular Analytics service.

### More...
Additionally, the Podfile notes a few optional libraries that you may find useful:

* [SZTextView](https://github.com/glaszig/SZTextView), a UITextView replacement which gives you: a placeholder.
* [DZNEmptyDataSet](https://github.com/dzenbot/DZNEmptyDataSet), A drop-in UITableView/UICollectionView superclass  
category for showing empty datasets.
* [IHKeyboardAvoiding](https://github.com/IdleHandsApps/IHKeyboardAvoiding), An elegant solution for keeping any  
UIView visible when the keyboard is being shown.


## Maintaining the Spirit
Plato will get you started on the right foot, but it's up to you not to mess it up! Here are some tips to  
stay in line with the spirit of the project.

Read up on the included and optional libraries. Most of them are very good at solving common problems, and  
you should become familiar with them. Ideally you should spend your time solving problems, not [solving problems around solving problems](http://www.chris-granger.com/2014/03/27/toward-a-better-programming/).

Here are some specific tips:

* Adding an external library? If there's a podspec for it, bring it in via Cocoapods. If there's not, consider writing one  
 and submitting it upstream.
    * There should almost never be a reason to check in third-party projects wholesale. If you need to modify someone else's  
    code, fork the repo and 
    include the fork in your Podfile with a direct [`:git` reference](http://guides.cocoapods.org/syntax/podfile.html#pod).


## License
The real content and value of Plato is as a template; once you've created a new project with the initialization script,  
Plato leaves barely a trace. So, in most cases, the only licenses you need to worry about are those of the third-party   
software you've included.  
But anyway, should you want to deal with Plato itself, it's MIT licensed:

Copyright (c) 2015 Polonius

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and  
associated documentation files (the "Software"), to deal in the Software without restriction, including  
without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS  
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS  
OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,  
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR  
IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Acknowledgements

This project is based on the amazing work done by [Crush & Lovely](http://crushlovely.com/) with their [Amaro](https://github.com/crushlovely/Amaro) boilerplate
