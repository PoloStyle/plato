source 'https://github.com/CocoaPods/Specs.git'

platform :ios, '8.0'

# Define all pods to be used
def all_pods
    # UI Goodies
    pod 'SZTextView'            # UITextView replacement which gives you: a placeholder
    pod 'DZNEmptyDataSet'       # A drop-in UITableView/UICollectionView superclass category for showing empty datasets
    pod 'IHKeyboardAvoiding'    # An elegant solution for keeping any UIView visible when the keyboard is being shown

    # Networking
    pod 'AFNetworking'     # A delightful networking library
    pod 'Reachability'     # Network reachability status

    # Analytics 
    pod 'GoogleAnalytics'  # Most popular Analytics service

    # Utilities
    pod 'FormatterKit'     # For all your string formatting needs
    pod 'Asterism'         # Nice & fast collection operations
    pod 'DateTools'        # Datetime heavy lifting
end

# Add these pods to all app targets
target 'Plato' do
    all_pods
end
 
target 'Plato-ci' do
    all_pods
end

# Inform CocoaPods that we use some custom build configurations
# Leave this in place unless you've tweaked the project's targets and configurations.
xcodeproj 'Plato',
'Debug-QA'   => :debug,   'Debug-Prod'   => :debug, 'Debug-Stage'    => :debug,
'Release-QA'   => :release, 'Release-Stage'   => :release, 'Release-Prod' => :release


# After every installation, copy the license and settings plists over to our project
post_install do |installer|
    require 'fileutils'
    
    # Include all license info in project
    acknowledgements_plist = 'Pods/Target Support Files/Pods-Plato/Pods-Plato-Acknowledgements.plist'
    if Dir.exists?('Plato/Resources/Settings.bundle') && File.exists?(acknowledgements_plist)
        FileUtils.cp(acknowledgements_plist, 'Plato/Resources/Settings.bundle/Acknowledgements.plist')
    end
    
    # Adds macros for checking if a pod is available in the project
    environment_file = 'Pods/Target Support Files/Pods-Plato/Pods-Plato-environment.h'
    if File.exists?(environment_file)
        FileUtils.cp(environment_file, 'Plato/Helpers/Pods-Plato-Environment.h')
    end
end